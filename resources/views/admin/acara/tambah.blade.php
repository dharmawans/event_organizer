@extends('template.app_admin')

@section('title', 'Tambah acara')

@section('content')
  <form action="{{ route('save_acara') }}" method="post">
    {{csrf_field()}}
    @include('admin.acara.form_acara')
  </form>
@endsection
