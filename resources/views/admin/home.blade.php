@extends('template.app_admin')

@section('title', 'Admin page')

@section('content')

<table>
  <thead>
    <th>Nama Acara</th>
    <th>Kategori Acara</th>
    <th>Lokasi Acara</th>
    <th>Deskripsi Acara</th>
    <th>Tanggal Acara</th>
    <th>Waktu Acara</th>
    <th>Link Acara</th>
    <th>Foto Acara</th>
  </thead>
  <tbody>
    @foreach($acara as $key)
    <tr>
      <td>{{ $key->nama_event }}</td>
      <td>{{ $key->kategori_event }}</td>
      <td>{{ str_limit($key->lokasi_event, 5) }}</td>
      <td>{{ str_limit($key->keterangan_event, 10) }}</td>
      <td>{{ $key->tanggal_event }}</td>
      <td>{{ $key->waktu_event }}</td>
      <td>{{ str_limit($key->url_event, 10) }}</td>
      <td>{{ str_limit($key->foto_event, 15) }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection
