<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Diallovite - @yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
  </head>
  <body>
    <nav>
      <ul>
        <li class="uppercase bold">Logo</li>
        <li><a href="{{ route('tambah_acara') }}">Create Events</a></li>
        <li>List Events</li>
        <li>About</li>
      </ul>
    </nav>
    <header>
      <ul>
        <li><a href="#!"><img src="{{ asset('img/people.jpg') }}" alt=""> People Name</a></li>
      </ul>
    </header>
    <main>
      <div class="container">
        @yield('content')
      </div>
    </main>
    <script src="{{ asset('js/jquery.js') }}" charset="utf-8"></script>
    <script>
      $(document).ready(function () {

      });
    </script>
  </body>
</html>
