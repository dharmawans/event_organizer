@extends('template.app')

@section('title', 'Homepage')

@section('header')

@endsection

@section('content')
<div class="input-field">
  <input type="search" name="" value="" placeholder="Kategori">
  <input type="search" name="" value="" placeholder="Lokasi">
</div>
<section class="list_events">
  <div class="col s12 m6">
    <figure class="card">
      <img src="{{ asset('img/event1.png') }}" alt="">
      <figcaption>
        <p style="color: rgba(46,62,72,.6);">Wednesday, March 14, 16:00</p>
        <p class="bold" style="color: #2e3e48;">Nama Acara</p>
        <div>
          <a href="#">Lokasi Acara</a>
          <a href="#">Kategori Acara</a>
        </div>
      </figcaption>
    </figure>
  </div>
  <div class="col s12 m6">
    <figure class="card">
      <img src="{{ asset('img/event1.png') }}" alt="">
      <figcaption>
        <p style="color: rgba(46,62,72,.6);">Wednesday, March 14, 16:00</p>
        <p class="bold" style="color: #2e3e48;">Nama Acara</p>
        <div>
          <a href="#">Lokasi Acara</a>
          <a href="#">Kategori Acara</a>
        </div>
      </figcaption>
    </figure>
  </div>
  <div class="col s12 m6">
    <figure class="card">
      <img src="{{ asset('img/event1.png') }}" alt="">
      <figcaption>
        <p style="color: rgba(46,62,72,.6);">Wednesday, March 14, 16:00</p>
        <p class="bold" style="color: #2e3e48;">Nama Acara</p>
        <div>
          <a href="#">Lokasi Acara</a>
          <a href="#">Kategori Acara</a>
        </div>
      </figcaption>
    </figure>
  </div>
</section>
@endsection
