<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Diallovite - <?php echo $__env->yieldContent('title'); ?></title>
    <link rel="stylesheet" href="<?php echo e(asset('css/admin/admin.css')); ?>">
  </head>
  <body>
    <nav>
      <ul>
        <li class="uppercase bold">Logo</li>
        <li><a href="<?php echo e(route('tambah_acara')); ?>">Create Events</a></li>
        <li>List Events</li>
        <li>About</li>
      </ul>
    </nav>
    <header>
      <ul>
        <li><a href="#!"><img src="<?php echo e(asset('img/people.jpg')); ?>" alt=""> People Name</a></li>
      </ul>
    </header>
    <main>
      <div class="container">
        <?php echo $__env->yieldContent('content'); ?>
      </div>
    </main>
    <script src="<?php echo e(asset('js/jquery.js')); ?>" charset="utf-8"></script>
    <script>
      $(document).ready(function () {

      });
    </script>
  </body>
</html>
