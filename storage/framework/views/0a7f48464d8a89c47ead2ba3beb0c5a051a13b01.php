<?php $__env->startSection('title', 'Admin page'); ?>

<?php $__env->startSection('content'); ?>

<table>
  <thead>
    <th>Nama Acara</th>
    <th>Kategori Acara</th>
    <th>Lokasi Acara</th>
    <th>Deskripsi Acara</th>
    <th>Tanggal Acara</th>
    <th>Waktu Acara</th>
    <th>Link Acara</th>
    <th>Foto Acara</th>
  </thead>
  <tbody>
    <?php $__currentLoopData = $acara; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
      <td><?php echo e($key->nama_event); ?></td>
      <td><?php echo e($key->kategori_event); ?></td>
      <td><?php echo e(str_limit($key->lokasi_event, 5)); ?></td>
      <td><?php echo e(str_limit($key->keterangan_event, 10)); ?></td>
      <td><?php echo e($key->tanggal_event); ?></td>
      <td><?php echo e($key->waktu_event); ?></td>
      <td><?php echo e(str_limit($key->url_event, 10)); ?></td>
      <td><?php echo e(str_limit($key->foto_event, 15)); ?></td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </tbody>
</table>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.app_admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>