<?php $__env->startSection('title', 'Tambah Acara'); ?>

<?php $__env->startSection('content'); ?>
<form action="index.html" method="post">
  <label for="">Nama event</label>
  <input type="text" class="" name="nama_event" placeholder="Nama event">
  <label for="">Lokasi event</label>
  <input type="text" class="" name="lokasi_event" placeholder="Lokasi event">
  <label for="">Kategori event</label>
  <input type="text" class="" name="kategori_event" placeholder="Kategori event">
  <label for="">Waktu event</label>
  <input type="text" name="waktu_event" placeholder="waktu event">
  <label for="">Cover event</label>
  <input type="file" name="">
  <label for="">Url event</label>
  <input type="text" name="" placeholder="Url event">
  <label for="">Tanggal event</label>
  <input type="text" name="" placeholder="Tanggal event">
  <label for="">Keterangan event</label>
  <textarea name="" rows="8" placeholder="Keterangan event"></textarea>
</form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.app_admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>