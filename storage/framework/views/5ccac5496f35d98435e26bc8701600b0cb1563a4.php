<?php $__env->startSection('title', 'Tambah acara'); ?>

<?php $__env->startSection('content'); ?>
  <form action="<?php echo e(route('save_acara')); ?>" method="post">
    <?php echo e(csrf_field()); ?>

    <?php echo $__env->make('admin.acara.form_acara', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.app_admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>