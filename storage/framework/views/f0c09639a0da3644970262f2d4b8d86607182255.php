<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Diallovite - <?php echo $__env->yieldContent('title'); ?></title>
    <link rel="stylesheet" href="<?php echo e(asset('css/organizer.css')); ?>">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
  </head>
  <body>
    <nav>
      <ul class="container">
        <li class="bold uppercase">
          <a href="" class="no-pad-left">Logo</a>
          <a href="javascript:void(0);" class="menu-icon"><i class="fas fa-bars"></i></a>
        </li>
        <li><a href="">Browse Events</a></li>
        <li><a href="">About</a></li>
        <?php if(auth()->guard()->guest()): ?>
        <li><a href="<?php echo e(url('/login')); ?>">Login</a></li>
        <?php else: ?>
        <li class="dropdown">
          <a href="javascript:void(0);"><?php echo e(Auth::user()->name); ?>

            <i class="fas fa-caret-down" style="padding-left: 10px;"></i>
          </a>
          <ul>
            <li><a href="profile.php">View Profile</a></li>
            <li>
              <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
              <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                  <?php echo e(csrf_field()); ?>

              </form>
            </li>
          </ul>
        </li>
        <?php endif; ?>
      </ul>
    </nav>
    <header>
      <?php echo $__env->yieldContent('header'); ?>
    </header>
    <main>
      <div class="container">
        <?php echo $__env->yieldContent('content'); ?>
      </div>
    </main>
    <footer>
      <div class="container">
        <p>Copyright &copy; <?php echo date("Y"); ?> Diallovite | All Rights Reserved.</p>
        <ul>
          <li><a href="facebook.com" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
          <li><a href="twitter.com" target="_blank"><i class="fab fa-twitter"></i></a></li>
          <li><a href="www.linkedin.com"><i class="fab fa-linkedin-in"></i></a></li>
        </ul>
      </div>
    </footer>
    <script src="<?php echo e(asset('js/jquery.js')); ?>"></script>
    <script>
    $(document).ready(function() {
      $(".menu-icon").click(function() {
        $("nav > ul").toggleClass("active");
      });
      $("nav ul li.dropdown").click(function() {
        $("nav ul li.dropdown").toggleClass("active");
      });
      $("main, header, footer").click(function() {
        $("nav ul li.dropdown").removeClass("active");
      });
    });
    </script>
  </body>
</html>
