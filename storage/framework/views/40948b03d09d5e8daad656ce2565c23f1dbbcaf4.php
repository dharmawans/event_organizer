<div class="input-field">
  <label for="nama_acara">Nama Acara</label>
  <input type="text" id="nama_acara" name="nama_acara" placeholder="Nama acara">
</div>
<div class="input-field">
  <label for="kategori_acara">Kategori Acara</label>
  <input type="text" id="kategori_acara" name="kategori_acara" placeholder="Kategori acara">
</div>
<div class="input-field">
  <label for="lokasi_acara">Lokasi Acara</label>
  <input type="text" id="lokasi_acara" name="lokasi_acara" placeholder="Lokasi acara">
</div>
<div class="input-field">
  <label for="deskripsi_acara">Deskripsi Acara</label>
  <textarea id="deskripsi_acara" name="deskripsi_acara" rows="8" placeholder="Deskripsi Acara"></textarea>
</div>
<div class="input-field">
  <label for="tanggal_acara">Tanggal Acara</label>
  <input type="date" name="tanggal_acara" id="tanggal_acara">
</div>
<div class="input-field">
  <label for="waktu_acara">Waktu Acara</label>
  <input type="time" name="waktu_acara" id="waktu_acara">
</div>
<div class="input-field">
  <label for="cover_acara">Cover Acara</label>
  <input type="file" id="cover_acara" name="cover_acara">
</div>
<div class="input-field">
  <label for="link_acara">Link Acara</label>
  <input type="url" id="link_acara" name="link_acara" placeholder="Link acara">
</div>
<button type="submit" name="button">Submit button</button>
