<?php

use Illuminate\Database\Seeder;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::insert([
          [
            'name' => 'admin',
            'password' => bcrypt('admin123'),
            'email' => 'admin@admin.com',
            'role' => 'admin',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
          ],
        ]);
    }
}
