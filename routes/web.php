<?php

Route::get('/images/{filename}', function ($filename)
{
  $path = public_path('image') . '/' . $filename;
  $file = File::get($path);
  $type = File::mimeType($path);
  $response = Response::make($file);
  $response->header("Content-Type", $type);
  return $response;
});

Route::get('/welcome', function() {
  return view('welcome');
});

Route::get('/', function() {
  return view('homepage');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home_user');

Route::middleware(['admin'])->group(function () {
  Route::prefix('admin')->group(function (){
    Route::get('/home','AdminController@lihat_acara')->name('home_admin');
    Route::get('/create','AdminController@tambah_acara')->name('tambah_acara');
    Route::post('/save','AdminController@save_acara')->name('save_acara');
    Route::get('/edit/{id}','AdminController@edit_acara')->name('edit_acara');
    Route::post('/update','AdminController@update_acara')->name('update_acara');
  });
});
Route::middleware(['user'])->group(function () {
  Route::view('/homepage','homepage');
});
