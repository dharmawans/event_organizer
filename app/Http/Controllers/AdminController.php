<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\events;

class AdminController extends Controller
{
    // public function index()
    // {
    //     return view('admin.home');
    // }
    public function lihat_acara()
    {
      $data['acara'] = events::all();
      return view('admin.home')->with($data);
    }
    public function tambah_acara()
    {
      $data['acara'] = events::all();
      return view('admin.acara.tambah');
    }
    public function save_acara(Request $r)
    {
      $new = new events;
      $new->nama_event = $r->input('nama_acara');
      $new->kategori_event = $r->input('kategori_acara');
      $new->lokasi_event = $r->input('lokasi_acara');
      $new->keterangan_event = $r->input('deskripsi_acara');
      $new->tanggal_event = $r->input('tanggal_acara');
      $new->waktu_event = $r->input('waktu_acara');
      $new->foto_event = $r->input('cover_acara');
      $new->url_event = $r->input('link_acara');
      $new->save();
      return redirect()->route('lihat_acara');
    }

    public function edit_acara($id_event)
    {
        $data['acara'] = events::find($id_event);
        return view('admin.edit-acara')->with($data);
    }

    public function update_acara()
    {
        $a = events::find(Input::get('id_event'));
        $a->nama_event = Input::get('nama_acara');
        $a->kategori_event = Input::get('kategori_acara');
        $a->lokasi_event = Input::get('lokasi_acara');
        $a->keterangan_event = Input::get('deskripsi_acara');
        $a->tanggal_event = Input::get('tanggal_acara');
        $a->waktu_event = Input::get('waktu_acara');
        $a->url_event = Input::get('link_acara');
        $a->save();
        return redirect()->route('lihat_acara');
    }

}
