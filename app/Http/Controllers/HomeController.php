<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\events;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('homepage');
    }
    // public function lihat_acara_user()
    // {
    //   $data['acara'] = events::all();
    //   return view('homepage')->with($data);
    // }
}
